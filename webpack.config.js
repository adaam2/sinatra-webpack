const webpack = require('webpack');
const path = require('path');

module.exports = {
    context: __dirname,
    entry: [
      'webpack-dev-server/client?http://0.0.0.0:8081',
      'webpack/hot/only-dev-server',
      './app/entry.js'
    ],
    output: {
        path: __dirname + '/dist',
        filename: 'bundle.js'
    },
    resolve: {
      extensions: ['.js', '.jsx'],
    },
    module: {
      loaders: [
        { 
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          loaders: ["jsx-loader", "babel-loader"]
        },
        { 
          test: /\.jsx$/,
          exclude: /(node_modules|bower_components)/,
          loaders: ["babel-loader"]
        },
        {
          test: /\.scss$/,
          loaders: ["style-loader", "css-loader", "sass-loader"]
        }
      ]
    },
    plugins: [
      new webpack.HotModuleReplacementPlugin(),
      new webpack.LoaderOptionsPlugin({
         // test: /\.xxx$/, // may apply this only for some modules
         options: {
           sassLoader: {
            includePaths: [path.resolve(__dirname, "./some-folder")]
          }
         }
       })
    ]
};